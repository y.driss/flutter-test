import Button from "./components/Button";
import Header from "./components/Header";
import Item from "./components/Item";
import Navbar from "./components/Navbar";

function App() {

  return (
    <>
    
    <Navbar/>
    <div className="py-10 flex flex-col  items-center">      
    <Header title="Video tools. Online" subtitle="All-in-one easy-to-use online video tools" />
    <div className="flex flex-wrap object-center justify-center gap-3 my-8 ">
      <Item icon="R" label="Download Reels"/>
      <Item icon="R" label="Download Reels"/>
      <Item icon="R" label="Download Reels"/>
      <Item icon="R" label="Download Reels"/>
      <Item icon="R" label="Download Reels"/>
      <Item icon="R" label="Download Reels"/>
      <Item icon="R" label="Download Reels"/>
    </div>
    <Button/>
    </div>
    
    </>
  );
    
}

export default App;
