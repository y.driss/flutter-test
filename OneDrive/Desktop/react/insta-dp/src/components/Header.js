const Header = (props) => {
    return (
        <div className="w-full text-center">
            <div className="font-bold text-black text-5xl">{props.title}</div>
            <br/>
            <div className="font-light text-black text-xl">{props.subtitle}</div>
        </div>
    );
}

export default Header;