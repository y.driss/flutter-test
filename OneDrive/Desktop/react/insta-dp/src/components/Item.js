const Item = (props) => {
    return (
        <div className="transition ease-in-out delay-150 hover:-translate-y-1 w-full md:w-40 py-5 md:py-10 shadow-xl rounded-lg text-center">
            <h1>{props.icon}</h1>
            <h1>{props.label}</h1>
        </div>
    );
}

export default Item;