# SOAR Mobile

This this the Flutter part from a whole ML-based system for network threats detection and response.
This mobile app allows the admin to monitor their network through visualisation of some stastics (Attacks per protocols...etc), and see the active/deactive devices in the network in real time using web sockets .

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
