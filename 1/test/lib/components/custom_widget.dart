import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test/clippers/my_custom_clipper2.dart';

class CustomWidget extends StatelessWidget {
  const CustomWidget({
    Key? key,
    required this.title,
    required this.firstText,
    required this.secondText,
    required this.thirdText,
    required this.firstPic,
    required this.secondPic,
    required this.thirdPic,
  }) : super(key: key);

  final String title;
  final String firstText;
  final String secondText;
  final String thirdText;
  final String firstPic;
  final String secondPic;
  final String thirdPic;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 300,
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 30,
              color: Color(0xFF4A5568),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        const SizedBox(
          height: 60,
        ),
        Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                width: (1.sw > 1000) ? 0.7.sw : 0.9.sw,
                child: Wrap(
                  spacing: 20,
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Stack(
                      children: [
                        if (1.sw > 1000)
                          Container(
                            height: 208,
                            width: 208,
                            decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(0xFFF7FAFC),
                            ),
                          ),
                        FittedBox(
                          child: RichText(
                            text: TextSpan(
                                text: '1.',
                                style: const TextStyle(
                                  fontSize: 130,
                                  color: Color(0xFF718096),
                                ),
                                children: [
                                  TextSpan(
                                    text: firstText,
                                    style: const TextStyle(
                                      fontSize: 30,
                                      color: Color(0xFF718096),
                                    ),
                                  )
                                ]),
                          ),
                        ),
                      ],
                    ),
                    SvgPicture.asset('assets/$firstPic.svg'),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 430,
              ),
              child: ClipPath(
                clipper: MyCustomClipper2(),
                child: Container(
                  padding: EdgeInsets.only(
                    right: 0.2.sw,
                    top: 80,
                    bottom: 80,
                  ),
                  width: 1.sw,
                  // height: 300,
                  decoration: const BoxDecoration(
                    color: Colors.amber,
                    gradient: LinearGradient(
                      colors: [
                        Color(0xFFE6FFFA),
                        Color(0xFFEBF4FF),
                      ],
                    ),
                  ),
                  child: Wrap(
                    alignment: WrapAlignment.center,
                    // spacing: 50,
                    children: [
                      SvgPicture.asset('assets/$secondPic.svg'),
                      FittedBox(
                        child: RichText(
                          text: TextSpan(
                            text: '2.',
                            style: const TextStyle(
                              fontSize: 130,
                              color: Color(0xFF718096),
                            ),
                            children: [
                              TextSpan(
                                text: secondText,
                                style: const TextStyle(
                                  fontSize: 30,
                                  color: Color(0xFF718096),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (1.sw > 1000)
              Positioned(
                top: 200,
                left: 0.3.sw,
                child: SvgPicture.asset(
                  'assets/Gruppe 1821.svg',
                  height: 350,
                ),
              ),
            if (1.sw > 1000)
              Positioned(
                top: 700,
                right: 0.4.sw,
                child: SvgPicture.asset('assets/Gruppe 1822.svg'),
              ),
            Container(
              padding: EdgeInsets.only(
                top: (1.sw > 1000) ? 860 : 900,
                right: 0.2.sw,
                bottom: 200,
              ),
              width: 1.sw,
              child: SizedBox(
                width: 0.5.sw,
                child: Wrap(
                  alignment: WrapAlignment.center,
                  runAlignment: WrapAlignment.center,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  spacing: 20,
                  children: [
                    Stack(
                      children: [
                        Container(
                          height: 250,
                          width: 250,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(0xFFF7FAFC),
                          ),
                        ),
                        FittedBox(
                          child: RichText(
                            text: TextSpan(
                              text: '3.',
                              style: const TextStyle(
                                fontSize: 130,
                                color: Color(0xFF718096),
                              ),
                              children: [
                                TextSpan(
                                  text: thirdText,
                                  style: const TextStyle(
                                    fontSize: 30,
                                    color: Color(0xFF718096),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    SvgPicture.asset(
                      'assets/$thirdPic.svg',
                      height: 300,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
