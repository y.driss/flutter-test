import 'package:flutter/material.dart';

class TabButton extends StatelessWidget {
  const TabButton({
    Key? key,
    required this.index,
    required this.currentIndex,
    required this.length,
    required this.label,
    required this.onTap,
  }) : super(key: key);

  final int index;
  final int currentIndex;
  final int length;
  final String label;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.horizontal(
        left:
            (index == 0) ? const Radius.circular(12) : const Radius.circular(0),
        right: (index == length - 1)
            ? const Radius.circular(12)
            : const Radius.circular(0),
      ),
      hoverColor: const Color(0x3081E6D9),
      onTap: onTap,
      child: Ink(
        decoration: BoxDecoration(
          border: Border.all(
            color: (currentIndex != index)
                ? const Color(0xFFCBD5E0)
                : Colors.transparent,
          ),
          borderRadius: BorderRadius.horizontal(
            left: (index == 0)
                ? const Radius.circular(12)
                : const Radius.circular(0),
            right: (index == length - 1)
                ? const Radius.circular(12)
                : const Radius.circular(0),
          ),
          color:
              (index == currentIndex) ? const Color(0xFF81E6D9) : Colors.white,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 7),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            color: (index == currentIndex)
                ? Colors.white
                : const Color(0xFF319795),
          ),
        ),
      ),
    );
  }
}
