import 'package:flutter/material.dart';

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path0 = Path();
    path0.moveTo(0, 0);
    path0.quadraticBezierTo(0, size.height * 0.7500000, 0, size.height);
    path0.cubicTo(
        size.width * 0.3595455,
        size.height * 1.0004167,
        size.width * 0.4181273,
        size.height * 0.9549000,
        size.width * 0.5575000,
        size.height * 0.9266667);
    path0.quadraticBezierTo(size.width * 0.6992636, size.height * 0.9032333,
        size.width, size.height * 0.9104167);
    path0.lineTo(size.width, 0);
    path0.lineTo(0, 0);
    return path0;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
