import 'package:flutter/material.dart';

class MyCustomClipper2 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path0 = Path();
    path0.moveTo(size.width * 0.0008333, size.height * 0.0675000);
    path0.quadraticBezierTo(size.width * 0.1787833, size.height * -0.0391500,
        size.width * 0.3667167, size.height * 0.0866500);
    path0.cubicTo(
        size.width * 0.4184583,
        size.height * 0.1212000,
        size.width * 0.6014000,
        size.height * 0.0945500,
        size.width * 0.6755917,
        size.height * 0.0554750);
    path0.quadraticBezierTo(size.width * 0.7380000, size.height * 0.0479750,
        size.width, size.height * 0.1716000);
    path0.lineTo(size.width, size.height * 0.7927500);
    path0.quadraticBezierTo(size.width * 0.6777667, size.height * 0.9018750,
        size.width * 0.6027083, size.height * 0.8368750);
    path0.cubicTo(
        size.width * 0.5510417,
        size.height * 0.7754250,
        size.width * 0.4028667,
        size.height * 0.7840500,
        size.width * 0.3010583,
        size.height * 0.8735750);
    path0.quadraticBezierTo(size.width * 0.2317333, size.height * 0.9394500, 0,
        size.height * 0.9783750);
    return path0;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
