// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart' as svg_provider;
import 'package:test/clippers/my_custom_clipper.dart';
import 'package:test/components/custom_widget.dart';
import 'package:test/components/tab_button.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  int index = 0;

  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      lowerBound: -1,
      upperBound: 1,
      duration: const Duration(milliseconds: 200),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(120),
        child: Container(
          height: 80,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color(0xFF319795),
                Color(0xFF3182CE),
              ],
            ),
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(10),
            ),
          ),
          alignment: Alignment.bottomCenter,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(10),
              ),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 5,
                  spreadRadius: 1,
                ),
              ],
            ),
            height: 70,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                  child: const Text(
                    'Login',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Builder(builder: (context) {
              if (1.sw > 1000) {
                return ClipPath(
                  clipper: MyCustomClipper(),
                  child: Container(
                    height: 430,
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                      colors: [
                        Color(0xFFEBF4FF),
                        Color(
                          (0xFFE6FFFA),
                        ),
                      ],
                    )),
                    padding: const EdgeInsets.only(
                      top: 40,
                      // left: 374,
                      // right: 620,
                      bottom: 70,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          width: 321,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              const FittedBox(
                                child: Text(
                                  'Deine Job website',
                                  style: TextStyle(
                                    fontSize: 50,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 50,
                                    vertical: 10,
                                  ),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7),
                                    gradient: const LinearGradient(
                                      colors: [
                                        Color(0xFF319795),
                                        Color(0xFF3182CE),
                                      ],
                                    ),
                                  ),
                                  child: const Text(
                                    'Kostenlos Registrieren',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 13,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 330,
                          width: 330,
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: svg_provider.Svg(
                                'assets/undraw_agreement_aajr.svg',
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                );
              } else {
                return Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    ClipPath(
                      clipper: MyCustomClipper(),
                      child: Container(
                        height: 430,
                        width: 1.sw,
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color(0xFFEBF4FF),
                              Color(0xFFE6FFFA),
                            ],
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 90,
                                vertical: 20,
                              ),
                              child: Text(
                                'Deine Job website',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 50,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Expanded(
                              child: SvgPicture.asset(
                                'assets/undraw_agreement_aajr.svg',
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 140,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(8, 0, 0, 0),
                            blurRadius: 5,
                            spreadRadius: 0,
                            offset: Offset(0, -8),
                          ),
                        ],
                        borderRadius: BorderRadius.vertical(
                          top: Radius.circular(10),
                        ),
                      ),
                      padding: const EdgeInsets.only(
                        left: 15,
                        right: 15,
                        top: 17,
                        bottom: 65,
                      ),
                      child: GestureDetector(
                        onTap: () {},
                        child: Container(
                          height: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(7),
                            gradient: const LinearGradient(
                              colors: [
                                Color(0xFF319795),
                                Color(0xFF3182CE),
                              ],
                            ),
                          ),
                          child: const Center(
                            child: Text(
                              'Kostenlos Registrieren',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              }
            }),
            const SizedBox(
              height: 60,
            ),
            AnimatedBuilder(
              animation: controller,
              builder: (context, child) {
                return Transform.translate(
                  offset: (1.sw > 600)
                      ? const Offset(0, 0)
                      : Offset(-1 * 100 * ((controller.value)), 0),
                  child: Transform.scale(
                    scale: (1.sw > 600) ? 1 : 1.5,
                    child: child,
                  ),
                );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  TabButton(
                    index: 0,
                    onTap: () {
                      setState(() {
                        index = 0;
                        controller.animateTo(index - 1);
                      });
                    },
                    currentIndex: index,
                    length: 3,
                    label: 'Arbeitnehmer',
                  ),
                  TabButton(
                    index: 1,
                    onTap: () {
                      setState(() {
                        index = 1;
                        controller.animateTo(index - 1);
                      });
                    },
                    currentIndex: index,
                    length: 3,
                    label: 'Arbeitgeber',
                  ),
                  TabButton(
                    onTap: () {
                      setState(() {
                        index = 2;
                        controller.animateTo(index - 1);
                      });
                    },
                    index: 2,
                    currentIndex: index,
                    length: 3,
                    label: 'Temporärbüro',
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 60,
            ),
            widgets[index]
          ],
        ),
      ),
    );
  }
}

List<Widget> widgets = [
  const CustomWidget(
    title: 'Drei einfache Schritte zu deinem neuen Job',
    firstText: 'Erstellen dein Lebenslauf',
    firstPic: 'undraw_Profile_data_re_v81r',
    secondPic: 'undraw_task_31wc',
    secondText: 'Erstellen dein Lebenslauf',
    thirdText: 'Mit nur einem Klick bewerben',
    thirdPic: 'undraw_personal_file_222m',
  ),
  const CustomWidget(
    title: 'Drei einfache Schritte zu deinem neuen Mitarbeiter',
    firstText: 'Erstellen dein Unternehmensprofil',
    firstPic: 'undraw_Profile_data_re_v81r',
    secondPic: 'undraw_about_me_wa29',
    secondText: 'Erstellen ein Jobinserat',
    thirdText: 'Wähle deinen neuen Mitarbeiter aus',
    thirdPic: 'undraw_swipe_profiles1_i6mr',
  ),
  const CustomWidget(
    title: 'Drei einfache Schritte zur Vermittlung neuer Mitarbeiter',
    firstText: 'Erstellen dein Unternehmensprofil',
    firstPic: 'undraw_Profile_data_re_v81r',
    secondPic: 'undraw_job_offers_kw5d',
    secondText: 'Erhalte Vermittlungs- angebot von Arbeitgeber',
    thirdText: 'Vermittlung nach Provision oder Stundenlohn',
    thirdPic: 'undraw_business_deal_cpi9',
  ),
];
