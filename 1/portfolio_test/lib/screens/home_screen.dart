import 'dart:math';
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:portfolio_test/Components/animated_paragraph.dart';
import 'package:portfolio_test/Components/header.dart';
import 'package:portfolio_test/Components/hover_widget.dart';
import 'package:portfolio_test/Components/slide_transition.dart';
import 'package:portfolio_test/Models/navigation_item.dart';
import 'package:portfolio_test/components/custom_anim_button.dart';
import 'package:portfolio_test/components/titled_text.dart';
import 'package:portfolio_test/controllers/controller.dart';
import 'package:portfolio_test/screens/portfolio_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key? key,
    required this.toSecondScreen,
  }) : super(key: key);

  final bool toSecondScreen;

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late ScrollController scrollController;

  PortfolioController portfolioController = Get.find<PortfolioController>();

  @override
  void initState() {
    super.initState();

    scrollController = ScrollController();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(
        milliseconds: 1300,
      ),
    );

    scrollController.addListener(() {
      if ((scrollController.position.pixels > 50) &&
          (scrollController.position.userScrollDirection ==
              ScrollDirection.reverse)) {
        if (controller.value == 0) {
          controller.forward();
        }
      }
      if ((scrollController.position.pixels < 600) &&
          (scrollController.position.userScrollDirection ==
              ScrollDirection.forward)) {
        if (controller.value == 1) {
          controller.reverse();
        }
      }
    });
    scrollController.addListener(() {
      setState(() {});
    });

    if (widget.toSecondScreen) {
      Future.delayed(const Duration(seconds: 1)).then((value) {
        controller.forward();
        scrollController.animateTo(
          745,
          duration: const Duration(
            seconds: 1,
          ),
          curve: Curves.ease,
        );
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Title(
      color: Colors.white,
      title: 'Cinex Porfolio',
      child: Scaffold(
        backgroundColor: const Color(0xFF1E0E2E),
        body: Stack(
          children: [
            ShaderMask(
              shaderCallback: (rectangle) {
                return const LinearGradient(
                  colors: [
                    Color(0xFF000000),
                    Colors.transparent,
                    Color(0xFF000000),
                    // Color(0xFF1E0E2E),
                  ],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ).createShader(
                  Rect.fromLTRB(
                    rectangle.width,
                    rectangle.height,
                    0,
                    0,
                  ),
                );
              },
              blendMode: BlendMode.dstOut,
              child: Opacity(
                opacity: 0.67 *
                    (1 -
                        CurvedAnimation(
                          parent: controller,
                          curve: Curves.easeInOutCirc,
                        ).value),
                child: Transform.scale(
                  scale: (scrollController.positions.isEmpty)
                      ? 1
                      : 1 + scrollController.position.pixels / 3000,
                  alignment: Alignment.topCenter,
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Image.asset(
                      'assets/background/background.gif',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Opacity(
              opacity: CurvedAnimation(
                parent: controller,
                curve: Curves.easeInOutCirc,
              ).value,
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: ShaderMask(
                  shaderCallback: (rectangle) {
                    return const LinearGradient(
                      colors: [
                        Color.fromARGB(100, 203, 3, 87),
                        Color.fromARGB(100, 30, 14, 46),
                      ],
                    ).createShader(
                      Rect.fromLTRB(
                        rectangle.width,
                        rectangle.height,
                        0,
                        0,
                      ),
                    );
                  },
                  blendMode: BlendMode.modulate,
                  child: Image.asset(
                    'assets/background/personal_image.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Transform.translate(
                    offset: Offset(
                      0,
                      -580 *
                          CurvedAnimation(
                            parent: controller,
                            curve: Curves.easeInOutCirc,
                          ).value,
                    ),
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: const EdgeInsets.all(30),
                        child: Column(
                          children: [
                            const Spacer(
                              flex: 1,
                            ),
                            Expanded(
                              flex: 2,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Spacer(),
                                  Expanded(
                                    flex: 5,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const Expanded(
                                          flex: 3,
                                          child: Text(
                                            'Hi, my name is',
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w200,
                                              wordSpacing: 5,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        const Expanded(
                                          flex: 8,
                                          child: Text(
                                            'Driss Yassine.',
                                            style: TextStyle(
                                              // height: 0.6,
                                              fontSize: 72,
                                              fontWeight: FontWeight.normal,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 8,
                                          child: RichText(
                                            text: TextSpan(
                                                text: 'I build',
                                                style: const TextStyle(
                                                  fontFamily: 'ProductSans',
                                                  textBaseline:
                                                      TextBaseline.ideographic,
                                                  fontSize: 72,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white60,
                                                ),
                                                children: [
                                                  TextSpan(
                                                    text: ' things ',
                                                    style: TextStyle(
                                                      textBaseline: TextBaseline
                                                          .ideographic,
                                                      fontSize: 72,
                                                      foreground: Paint()
                                                        ..style =
                                                            PaintingStyle.stroke
                                                        ..strokeWidth = 1
                                                        ..color =
                                                            Colors.white60,
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text: 'for mobile.',
                                                  ),
                                                ]),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 50,
                                        ),
                                        // Spacer(),
                                        CustomAnimButton(
                                          text: 'SEE WORK',
                                          onTap: () {
                                            Navigator.push(
                                              context,
                                              SlideTransitionNav(
                                                widget: const PortfolioScreen(),
                                                offset: const Offset(1, 0),
                                                routeName: '/portfolio',
                                              ),
                                            );
                                          },
                                        ),
                                        // Spacer(),
                                      ],
                                    ),
                                  ),
                                  const Spacer(
                                    flex: 2,
                                  ),
                                  Expanded(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Text(
                                              DateTime.now()
                                                  .month
                                                  .toString()
                                                  .padLeft(2, '0'),
                                              style: const TextStyle(
                                                // height: 0.7,
                                                color: Colors.white,
                                                fontSize: 50,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Container(
                                              color: Colors.white,
                                              height: 4,
                                              // width: 60,
                                            ),
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          '/ ${DateTime.now().day.toString().padLeft(2, '0')}',
                                          style: const TextStyle(
                                            color: Colors.pink,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ))
                                ],
                              ),
                            ),
                            const Spacer(),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width / 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Spacer(),
                        const Text(
                          "WHO",
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Transform(
                          transform: Matrix4.skewY(-0.2),
                          child: Container(
                            width: 24 *
                                CurvedAnimation(
                                  parent: controller,
                                  curve: Curves.easeInOutCirc,
                                ).value,
                            height: 7,
                            color: Colors.pink,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        AnimatedParagraph(
                          controller: controller,
                          stops: const [
                            0,
                            0.5,
                            0.2,
                            0.7,
                            0.4,
                            0.9,
                            0.6,
                            1,
                          ],
                          children: [
                            const Text(
                              'Create the opportunity.',
                              style: TextStyle(
                                height: 1.2,
                                fontSize: 40,
                                color: Colors.white,
                                fontFamily: 'ProductSans',
                                // fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              "${portfolioController.bossInfo.value.bio}\n",
                              style: const TextStyle(
                                fontSize: 20,
                                color: Colors.white70,
                                fontFamily: 'ProductSans',
                                // fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                TitledText(
                                  title: 'Skills',
                                  texts:
                                      portfolioController.bossInfo.value.skills,
                                ),
                                TitledText(
                                  title: 'Software & Tools',
                                  texts:
                                      portfolioController.bossInfo.value.tools,
                                ),
                                TitledText(
                                  title: 'Interests',
                                  texts: portfolioController
                                      .bossInfo.value.interests,
                                ),
                              ],
                            ),
                          ],
                        ),
                        const Spacer(),
                        const Text(
                          'Made with ❤️ by Yassine DRISS, 2022',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w100,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Header(
              navigationItems: [
                NavigationItem(
                  label: 'INTRO',
                  onTap: () {
                    if (scrollController.offset == 745) {
                      controller.reverse();
                      scrollController.animateTo(
                        0,
                        duration: const Duration(
                          seconds: 1,
                        ),
                        curve: Curves.ease,
                      );
                    }
                  },
                ),
                NavigationItem(
                  label: 'WHO',
                  onTap: () {
                    if (scrollController.offset < 745) {
                      controller.forward();
                      scrollController.animateTo(
                        745,
                        duration: const Duration(
                          seconds: 1,
                        ),
                        curve: Curves.ease,
                      );
                    }
                  },
                ),
                NavigationItem(
                  label: 'WORK',
                  onTap: () {
                    Navigator.push(
                      context,
                      SlideTransitionNav(
                        widget: const PortfolioScreen(),
                        offset: const Offset(1, 0),
                        routeName: '/portfolio',
                      ),
                    );
                  },
                ),
              ],
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(
                vertical: 30.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Transform.translate(
                    offset: Offset(
                        0,
                        (1 -
                                CurvedAnimation(
                                  parent: controller,
                                  curve: Curves.easeInOutCirc,
                                ).value) *
                            -60),
                    child: IconButton(
                      onPressed: () {
                        controller.reverse();
                        scrollController.animateTo(
                          0,
                          duration: const Duration(
                            seconds: 1,
                          ),
                          curve: Curves.ease,
                        );
                      },
                      icon: const Icon(
                        Icons.keyboard_arrow_up_rounded,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Transform.translate(
                    offset: Offset(
                        0,
                        CurvedAnimation(
                              parent: controller,
                              curve: Curves.easeInOutCirc,
                            ).value *
                            60),
                    child: IconButton(
                      onPressed: () {
                        controller.forward();
                        scrollController.animateTo(
                          745,
                          duration: const Duration(
                            seconds: 1,
                          ),
                          curve: Curves.ease,
                        );
                      },
                      icon: const Icon(
                        Icons.keyboard_arrow_down_rounded,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 50,
              child: Column(
                children: [
                  HoverWidget(
                    child: const Icon(
                      FontAwesomeIcons.github,
                      color: Colors.white,
                    ),
                    onTap: () {
                      html.window.open(
                        portfolioController.bossInfo.value.githubUrl,
                        'Github',
                      );
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  HoverWidget(
                    child: const Icon(
                      FontAwesomeIcons.instagram,
                      color: Colors.white,
                    ),
                    onTap: () {
                      html.window.open(
                        portfolioController.bossInfo.value.instagramUrl,
                        'Instagram',
                      );
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  HoverWidget(
                    child: const Icon(
                      FontAwesomeIcons.linkedin,
                      color: Colors.white,
                    ),
                    onTap: () {
                      html.window.open(
                        portfolioController.bossInfo.value.linkedinUrl,
                        'Linkedin',
                      );
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  HoverWidget(
                    child: const Icon(
                      FontAwesomeIcons.facebook,
                      color: Colors.white,
                    ),
                    onTap: () {
                      html.window.open(
                        portfolioController.bossInfo.value.facebookUrl,
                        'Facebook',
                      );
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Container(
                    height: 100,
                    width: 1.5,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              right: 50,
              child: Transform.rotate(
                angle: pi / 2,
                alignment: Alignment.centerRight,
                origin: const Offset(-10, 10),
                child: Row(
                  children: [
                    HoverWidget(
                        child: const Text(
                          'its.cinex@gmail.com',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                        onTap: () {}),
                    const SizedBox(
                      width: 15,
                    ),
                    Container(
                      width: 100,
                      height: 1.5,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SocialMediaContacts extends StatelessWidget {
  const SocialMediaContacts({
    Key? key,
    this.color = Colors.white,
  }) : super(key: key);

  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          FontAwesomeIcons.facebook,
          color: color ?? Colors.white,
          semanticLabel: 'Facebook',
        ),
        const SizedBox(
          width: 12,
        ),
        Icon(
          FontAwesomeIcons.github,
          color: color ?? Colors.white,
          semanticLabel: 'Github',
        ),
        const SizedBox(
          width: 12,
        ),
        Icon(
          FontAwesomeIcons.instagram,
          color: color ?? Colors.white,
          semanticLabel: 'Instagram',
        ),
        const SizedBox(
          width: 12,
        ),
        Icon(
          FontAwesomeIcons.telegram,
          color: color ?? Colors.white,
          semanticLabel: 'Telegram',
        ),
        const SizedBox(
          width: 12,
        ),
        Icon(
          FontAwesomeIcons.whatsapp,
          color: color ?? Colors.white,
          semanticLabel: 'Whatsapp',
        ),
      ],
    );
  }
}
