import 'package:get/get.dart';
import 'package:portfolio_test/controllers/controller.dart';

class InitBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PortfolioController());
  }
}
