import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:portfolio_test/binding.dart';
import 'package:portfolio_test/screens/home_screen.dart';

void main() {
  runApp(const Root());
}

class Root extends StatelessWidget {
  const Root({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      color: Colors.red,
      home: const HomeScreen(
        toSecondScreen: false,
      ),
      theme: ThemeData(
        fontFamily: 'ProductSans',
      ),
      initialBinding: InitBinding(),
    );
  }
}
